#include "hexagon.h"
#include "MathUtils.h"
#include "shape.h"

Hexagon::Hexagon(const double len, const std::string name, const std::string color) : Shape(name, color)
{
	setLength(len);
}

void Hexagon::setLength(const double length)
{
	if (length < 0)
	{
		throw((int)length);
	}
	_length = length;
}

double Hexagon::CalArea() const
{
	return MathUtils::calHexagonArea(_length);
}

void Hexagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl << "Length is " << _length << std::endl
		<< "area is " << CalArea() << std::endl;
}
