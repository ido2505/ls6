#include "MathUtils.h"



MathUtils::MathUtils()
{
}

int MathUtils::calPentagonArea(double len)
{
	int ans = sqrt(25 + (10 * sqrt(5)));
	ans = ans / 4;
	ans *= len * len;
	return ans;
}

int MathUtils::calHexagonArea(double len)
{
	int ans = len * len * 3;
	ans = ans / 2;
	ans *= sqrt(3);
	return ans;
}


MathUtils::~MathUtils()
{
}
