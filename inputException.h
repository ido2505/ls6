#pragma once
#include <exception>

class inputException : public std::exception
{
public:
	virtual const char* error() const
	{
		return "wrong input type";
	}
};
