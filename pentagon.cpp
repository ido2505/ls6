#include "shape.h"
#include "pentagon.h"
#include "shapeException.h"
#include <string>
#include <iostream>
#include "MathUtils.h"

Pentagon::Pentagon(const double length, std::string name, std::string color)  : Shape(name, color)
{
	setLength(length);
}

void Pentagon::setLength(const double length)
{
	if (length < 0) 
	{
		throw((int)length);
	}
	_length = length;
}

void Pentagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl << "Length is " << _length << std::endl
		<< "area is " << CalArea() << std::endl;
}

double Pentagon::CalArea() const
{
	return MathUtils::calPentagonArea(_length);
}