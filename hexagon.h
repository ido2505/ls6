#include <string>
#include "shape.h"
class Hexagon : public Shape
{
public:
	Hexagon(const double, const std::string, const std::string);

	void setLength(const double);

	void draw() const;
	double CalArea() const;

private:
	double _length;
};

