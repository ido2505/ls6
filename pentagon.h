#pragma once
#include "shape.h"
class Pentagon : public Shape
{
public:
	Pentagon(const double, std::string, std::string);

	void setLength(const double);

	void draw(); 
	double CalArea() const;

private:
	double _length;
};

