#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include <cstddef>
#include <cstring>
#include "shapeException.h"
#include "inputException.h"
#include "hexagon.h"
#include "pentagon.h"

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180, length; int height = 0, width = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Hexagon hex(length, nam, col);
	Pentagon pen(length, nam, col);


	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape *ptrhex = &hex;
	Shape *ptrpen = &pen;



	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p', pentagon = 'n', hexagon = 'h'; char* shapetype = new char();
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, n = pentagon, h = hexagon" << std::endl;
		std::cin >> shapetype;
		if (std::cin.fail())
		{
			std::cin.clear();//This corrects the stream.
			std::cin.ignore();
			throw(inputException());
			 
			//std::cin.ignore();
		}
		if (strlen(shapetype) > 1) 
		{
			std::cout << "Warning - Dont try to build more than one shape at once" << std::endl;
		}

		try
		{

			switch (shapetype[0]) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				if (std::cin.fail())
				{
					std::cin.clear();//This corrects the stream.
					std::cin.ignore();
					throw(inputException());
					 
					//std::cin.ignore();
				}

				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail())
				{
					std::cin.clear(); //This corrects the stream.
					std::cin.ignore();
					throw(inputException());
					
					//std::cin.ignore();
				}

				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail())
				{
					std::cin.clear(); //This corrects the stream.
					std::cin.ignore();
					throw(inputException());
					
					//std::cin.ignore();
				}

				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				if (std::cin.fail()) 
				{
					std::cin.clear(); //This corrects the stream.
					std::cin.ignore();
					throw(inputException());
					
					//std::cin.ignore();
				}

				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();

			case 'n':
				std::cout << "enter name, color, length" << std::endl;
				std::cin >> nam >> col >> length;
				if (std::cin.fail())
				{
					std::cin.clear(); //This corrects the stream.
					std::cin.ignore();
					throw(inputException());

					//std::cin.ignore();
				}

				pen.setName(nam);
				pen.setColor(col);
				pen.setLength(length);
				ptrpen->draw();

			case 'h':
				std::cout << "enter name, color, length" << std::endl;
				std::cin >> nam >> col >> length;
				if (std::cin.fail())
				{
					std::cin.clear(); //This corrects the stream.
					std::cin.ignore();
					throw(inputException());

					//std::cin.ignore();
				}

				hex.setName(nam);
				hex.setColor(col);
				hex.setLength(length);
				ptrhex->draw();

			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin.get() >> x;
			if (std::cin.fail())
			{
				std::cin.clear(); //This corrects the stream.
				std::cin.ignore();
				throw(inputException());
				
				//
			}

		}
		catch (const inputException& er) 
		{
			std::cout << er.error() << std::endl;
		}
		catch (const int wrongNum) 
		{
			std::cout << "number is under zero or zero: " << wrongNum << std::endl;
		}
		catch (std::exception e)
		{
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



	system("pause");
	return 0;

}